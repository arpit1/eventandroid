package com.endive.eventplanner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import com.endive.eventplanner.R;
import com.endive.eventplanner.adapter.CategoriesGridAdapter;
import com.endive.eventplanner.pojo.CategoryListPojo;
import com.endive.eventplanner.pojo.SomethingInterestingDataPojo;
import com.endive.eventplanner.util.EventConstant;

import java.util.ArrayList;

public class CategoriesActivity extends BaseActivity implements View.OnClickListener {

    private CategoriesActivity ctx = this;
    private GridView category_name_grid;
    private ArrayList<CategoryListPojo> items;
    private CategoriesGridAdapter adapter = null;
    private ImageView map_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        initialize();
        setListener();
    }

    private void initialize() {
        items = new ArrayList<>();
        map_image = (ImageView) findViewById(R.id.map_image);
        category_name_grid = (GridView) findViewById(R.id.category_name_grid);
        items = getCategoryData().getData().getCategory();
        setHeader(getResources().getString(R.string.browse_categories));
        setAdapter(items);
    }

    private void setListener() {
        map_image.setOnClickListener(ctx);
    }

    @Override
    public void onClick(View view) {
        SomethingInterestingDataPojo data;
        switch (view.getId()) {
            case R.id.category_image:
                CategoryListPojo dataCat = (CategoryListPojo) view.getTag(R.string.data);
                navigateToSearchResult(dataCat.getId(), "category", dataCat.getCategory_name());
                break;

            case R.id.map_image:
                Intent intentMap = new Intent(ctx, MapActivity.class);
                intentMap.putExtra("data", items);
                startActivityForResult(intentMap, EventConstant.START_MAP);
                break;

            default:
                break;
        }
    }

    private void navigateToSearchResult(String cat_id, String type, String header) {
        Intent intent = new Intent(ctx, SearchResultActivity.class);
        intent.putExtra("keyword", cat_id);
        intent.putExtra("type", type);
        intent.putExtra("api", "Search");
        intent.putExtra("header", header);
        intent.putExtra("from", "categories");
        startActivity(intent);
    }

    private void setAdapter(ArrayList<CategoryListPojo> arr) {
        adapter = new CategoriesGridAdapter(ctx, ctx.getCategoryData().getData().getCategory());
        category_name_grid.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
