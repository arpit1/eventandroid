package com.endive.eventplanner.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.endive.eventplanner.R;
import com.endive.eventplanner.pojo.PaymentNonceResultPojo;
import com.endive.eventplanner.util.EventConstant;

public class OrderCompletedActivity extends BaseActivity {

    private OrderCompletedActivity ctx = this;
    private TextView new_order;
    private ImageView home;
    private PaymentNonceResultPojo paymentResult;
    private LinearLayout yes, no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_completed);

        setHeader(getResources().getString(R.string.order_completed));
        initialize();
        setListener();
    }

    private void initialize() {
        paymentResult = (PaymentNonceResultPojo) getIntent().getSerializableExtra("payment_response");

        ImageView nav_invisible = (ImageView) findViewById(R.id.nav_invisible);
        nav_invisible.setVisibility(View.GONE);

        home = (ImageView) findViewById(R.id.back);
        home.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.home));

        new_order = (TextView) findViewById(R.id.header_skip);
        new_order.setVisibility(View.VISIBLE);

        yes = (LinearLayout) findViewById(R.id.yes);
        no = (LinearLayout) findViewById(R.id.no);

        new_order.setText(getResources().getString(R.string.new_order));
    }

    private void setListener() {
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home.performClick();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < EventConstant.ACTIVITIES.size(); i++) {
                    if (EventConstant.ACTIVITIES.get(i) != null)
                        EventConstant.ACTIVITIES.get(i).finish();
                }
                finish();
                Intent intent = new Intent(ctx, HomeActivity.class);
                startActivity(intent);
            }
        });

        new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < EventConstant.ACTIVITIES.size(); i++) {
                    Activity obj = EventConstant.ACTIVITIES.get(i);
                    if (obj != null && (obj.toString().contains("OrderSummaryActivity") || obj.toString().contains("FillTicketDetailsActivity") || obj.toString().contains("BuyTicketActivity"))) {
                        obj.finish();
                    }
                }
                finish();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < EventConstant.ACTIVITIES.size(); i++) {
                    if (EventConstant.ACTIVITIES.get(i) != null)
                        EventConstant.ACTIVITIES.get(i).finish();
                }
                Intent intent = new Intent(ctx, PlacedOrderDetailActivity.class);
                intent.putExtra("payment_response", paymentResult);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        home.performClick();
    }
}
