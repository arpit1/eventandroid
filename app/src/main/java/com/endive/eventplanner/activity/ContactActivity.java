package com.endive.eventplanner.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.endive.eventplanner.R;
import com.endive.eventplanner.model.ApiClient;
import com.endive.eventplanner.model.ApiInterface;
import com.endive.eventplanner.pojo.BasePojo;
import com.endive.eventplanner.pojo.ContactPojo;
import com.endive.eventplanner.util.ConnectionDetector;
import com.endive.eventplanner.util.EventConstant;
import com.endive.eventplanner.util.EventDialogs;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by upasna.mishra on 11/6/2017.
 */

public class ContactActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_email, tv_phone_no, tv_address, tv_fax_no;
    private ContactActivity ctx = this;
    private ConnectionDetector cd;
    private EventDialogs dialog;
    private LinearLayout lin_phone_no, lin_email;
    private EditText first_name, last_name, email, organization_name, mobile, message;
    private LinearLayout org, individual, submit;
    private ImageView radio_org, radio_individual;
    private String user_type = "1";
    private LinearLayout call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_activity);
        setHeader(getResources().getString(R.string.contact_us));
        cd = new ConnectionDetector(this);
        dialog = new EventDialogs(this);
        getContact();

        initialize();
        setListener();
    }

    private void initialize() {
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_phone_no = (TextView) findViewById(R.id.tv_phone_no);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_fax_no = (TextView) findViewById(R.id.tv_fax_no);
        lin_email = (LinearLayout) findViewById(R.id.lin_email);
        lin_phone_no = (LinearLayout) findViewById(R.id.lin_phone_no);

        org = (LinearLayout) findViewById(R.id.org);
        call = (LinearLayout) findViewById(R.id.call);
        individual = (LinearLayout) findViewById(R.id.individual);
        submit = (LinearLayout) findViewById(R.id.submit);
        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        organization_name = (EditText) findViewById(R.id.organization_name);
        mobile = (EditText) findViewById(R.id.mobile);
        message = (EditText) findViewById(R.id.message);
        radio_org = (ImageView) findViewById(R.id.radio_org);
        radio_individual = (ImageView) findViewById(R.id.radio_individual);
    }

    private void setListener() {
        lin_email.setOnClickListener(this);
        call.setOnClickListener(this);

        org.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_type = "1";
                radio_org.setImageResource(R.mipmap.circle_colour);
                radio_individual.setImageResource(R.mipmap.circle);
                organization_name.setVisibility(View.VISIBLE);
            }
        });

        individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_type = "2";
                radio_org.setImageResource(R.mipmap.circle);
                radio_individual.setImageResource(R.mipmap.circle_colour);
                organization_name.setVisibility(View.GONE);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateContactUsForm();
            }
        });
    }

    private void validateContactUsForm() {
        String firstName = first_name.getText().toString().trim();
        String lastName = last_name.getText().toString().trim();
        String email_val = email.getText().toString().trim();

        String org_name = organization_name.getText().toString().trim();
        String msg = message.getText().toString().trim();
        String mobile_val = mobile.getText().toString();

        dialog = new EventDialogs(ctx);


        if (firstName.equals("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.first_name_msg));
        } else if (lastName.equals("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.last_name_msg));
        } else if (organization_name.getVisibility() == View.VISIBLE && org_name.equals("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.org_name_msg));
        } else if (email_val.equals("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.email_id_msg));
        } else if (!email_val.equals("") && !isValidEmail(email_val)) {
            dialog.displayCommonDialog(getResources().getString(R.string.email_vaild_id_msg));
        } else if (!email_val.matches(EventConstant.EMAIL_REGEX)) {
            dialog.displayCommonDialog(getResources().getString(R.string.email_vaild_id_msg));
        } else if (mobile_val.equals("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.mobile_no_msg));
        } else if (!mobile_val.equals("") && !mobile_val.matches("[0-9]+")) {
            dialog.displayCommonDialog(getResources().getString(R.string.valid_mobile_no_msg));
        } else if (!mobile_val.equals("") && mobile_val.length() < 10) {
            dialog.displayCommonDialog(getResources().getString(R.string.no_between_msg));
        } else if (msg.equals("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.message_msg));
        } else {
            submitContactRequest(firstName, lastName, email_val, mobile_val, org_name, msg);
            hideSoftKeyboard();
        }
    }

    private void getContact() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = EventDialogs.showLoading(this);
            d.setCanceledOnTouchOutside(false);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ContactPojo> call = apiService.getContactDetails("Contact");
            call.enqueue(new Callback<ContactPojo>() {
                @Override
                public void onResponse(Call<ContactPojo> call, Response<ContactPojo> response) {
                    if (response.body() != null && response.body().getStatus_code().equals("1")) {
                        tv_email.setText(response.body().getData().getSupport_email());
                        tv_phone_no.setText(response.body().getData().getPhone_no());
                        tv_address.setText(response.body().getData().getAddress());
                        tv_fax_no.setText(response.body().getData().getFax_no());
                    } else {
                        dialog.displayCommonDialog(response.body().getError_message());
                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<ContactPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet_connection));
        }
    }

    private void submitContactRequest(String firstName, String lastName, String email, String phone, String org_name, String msg) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = EventDialogs.showLoading(this);
            d.setCanceledOnTouchOutside(false);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BasePojo> call = apiService.submitContactRequest("contactUs", firstName, lastName, org_name, email, phone, msg, user_type);
            call.enqueue(new Callback<BasePojo>() {
                @Override
                public void onResponse(Call<BasePojo> call, Response<BasePojo> response) {
                    if (response.body() != null && response.body().getStatus_code().equals("1")) {
                        displayToastLong(response.body().getMessage());
                        finish();
                    } else {
                        dialog.displayCommonDialog(response.body().getError_message());
                    }
                    d.dismiss();
                }

                @Override
                public void onFailure(Call<BasePojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lin_email:
                String email[] = {tv_email.getText().toString()};
                shareToGMail(email);
                break;
            case R.id.call:
                String number = tv_phone_no.getText().toString();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1000);
                } else {
                    startActivity(callIntent);
                }
                break;
        }
    }

    public void shareToGMail(String[] email) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.setType("text/plain");
        final PackageManager pm = getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivity(emailIntent);
    }
}
