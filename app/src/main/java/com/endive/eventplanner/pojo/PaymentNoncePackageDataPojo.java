package com.endive.eventplanner.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by arpit.jain on 12/19/2017.
 */

public class PaymentNoncePackageDataPojo implements Serializable {
    private String name;
    private String gender;
    private String gender_on_ticket;
    private String phone_on_ticket;
    private String email_on_ticket;
    private String booked_seat;
    private String age;
    private ArrayList<PaymentNoncePackageProductPojo> product_array;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<PaymentNoncePackageProductPojo> getProduct_array() {
        return product_array;
    }

    public void setProduct_array(ArrayList<PaymentNoncePackageProductPojo> product_array) {
        this.product_array = product_array;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender_on_ticket() {
        return gender_on_ticket;
    }

    public void setGender_on_ticket(String gender_on_ticket) {
        this.gender_on_ticket = gender_on_ticket;
    }

    public String getPhone_on_ticket() {
        return phone_on_ticket;
    }

    public void setPhone_on_ticket(String phone_on_ticket) {
        this.phone_on_ticket = phone_on_ticket;
    }

    public String getEmail_on_ticket() {
        return email_on_ticket;
    }

    public void setEmail_on_ticket(String email_on_ticket) {
        this.email_on_ticket = email_on_ticket;
    }

    public String getBooked_seat() {
        return booked_seat;
    }

    public void setBooked_seat(String booked_seat) {
        this.booked_seat = booked_seat;
    }
}
