package com.endive.eventplanner.pojo;

/**
 * Created by upasna.mishra on 11/6/2017.
 */

public class CouponPojo extends BasePojo {

    private CouponDataPojo data;

    public CouponDataPojo getData() {
        return data;
    }

    public void setData(CouponDataPojo data) {
        this.data = data;
    }
}
